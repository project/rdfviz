; Build and bootstrap a sample site with requirements for rdfvz
; No install profile, just download requirements.

api = 2
core = 7.x

projects[] = drupal

projects[] = rdfviz
projects[] = rel_links